package core;

public enum GroupEvent {
    CREATED,
    REMOVED
}
