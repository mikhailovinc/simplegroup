package core;

public interface KeyGenerator {
    Key getKey();
}
