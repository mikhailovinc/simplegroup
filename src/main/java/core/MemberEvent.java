package core;

public enum MemberEvent {
    LEAVE,
    JOIN,
    BLOCKED,
    UNBLOCKED
}
