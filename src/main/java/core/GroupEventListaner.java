package core;

public interface GroupEventListaner {
    void onGroupEvent(GroupEvent event, Key key);
}
