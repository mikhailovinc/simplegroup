package core;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class Group <R> {
    private List<Member> members = new ArrayList<>();
    private State<R> state;
    private List<Member> blackList = new ArrayList<>();
    private List<Member> whiteList = new ArrayList<>();
    private Strategy strategy;


    public Group(State<R> state, Strategy strategy) {
        this.state = state;
        this.strategy = strategy;
    }

    public void update(Member member, Object data, Class dataClazz) {
        state.update(member, data, dataClazz, this);
    }

    public R read(Member member) {
        return state.read(member);
    }

    public void addMember(Member member) {
        members.add(member);
    }

    public void removeMember(Member member) {
        members.remove(member);
    }

    public void block(Member member) {
        if (strategy == Strategy.OPEN) {
            blackList.add(member);
        } else {
            whiteList.remove(member);
        }
    }

    public void unblock(Member member) {
        if (strategy == Strategy.OPEN) {
            blackList.remove(member);
        } else {
            whiteList.add(member);
        }
    }

    public boolean isMemberAllowed(Member member) {
        if (strategy == Strategy.OPEN) {
            return !blackList.contains(member);
        } else {
            return whiteList.contains(member);
        }
    }

    public boolean isGroupEmpty(Key key) {
        return members.isEmpty();
    }
}
