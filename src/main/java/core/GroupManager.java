package core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupManager<R> {
    private Map<Key, Group<R>> groups = new HashMap<>();
    private List<GroupEventListaner> groupEventListaners = new ArrayList<>();
    private List<MemberEventListaner> memberEventListaners = new ArrayList<>();
    private KeyGenerator keyGenerator;
    private static GroupManager instance;

    private GroupManager() {}

    public static synchronized GroupManager getInstance() {
        if (instance == null) {
            instance = new GroupManager();
        }

        return instance;
    }

    public void addMemberEventListener(MemberEventListaner listaner) {
        memberEventListaners.add(listaner);
    }

    public void addGroupEventListener(GroupEventListaner listaner) {
        groupEventListaners.add(listaner);
    }

    public Key createGroup (Strategy strategy, State<R> state) {
        Key key = keyGenerator.getKey();
        groups.put(key, new Group<R>(state, strategy));

        for (GroupEventListaner listaner : groupEventListaners) {
            listaner.onGroupEvent(GroupEvent.CREATED, key);
        }
        return key;
    }

    public boolean joinGroup(Key key, Member member) {
        if (!groups.containsKey(key)) {
            return false;
        }

        Group group = groups.get(key);
        group.addMember(member);

        for (MemberEventListaner listaner : memberEventListaners) {
            listaner.onMemberEnent(MemberEvent.JOIN, member);
        }

        return true;
    }

    public void leaveGroup(Key key, Member member) {
        if (groups.containsKey(key)) {
            groups.get(key).removeMember(member);
        }

        if (isGroupEmpty(key)) {
            removeGroup(key);
        }

        for (MemberEventListaner listaner : memberEventListaners) {
            listaner.onMemberEnent(MemberEvent.LEAVE, member);
        }
    }

    public void removeGroup(Key key) {
        groups.remove(key);
        for (GroupEventListaner listaner : groupEventListaners) {
            listaner.onGroupEvent(GroupEvent.REMOVED, key);
        }
    }

    public void updateGrooupState(Key key, Member member, Object data, Class dataClass) {
        Group  group = groups.get(key);
        if (group != null) {
            group.update(member, data, dataClass);
        }
    }

    public R read(Key key, Member member) {
        Group<R> group = groups.get(key);
        return group.read(member);
    }

    public void blockMember(Key key, Member member) {
        Group  group = groups.get(key);
        if (group != null) {
            group.block(member);
        }
        for (MemberEventListaner listaner : memberEventListaners) {
            listaner.onMemberEnent(MemberEvent.BLOCKED, member);
        }
    }

    public void unblockMember(Key key, Member member) {
        Group  group = groups.get(key);
        if (group != null) {
            group.unblock(member);
        }

        for (MemberEventListaner listaner : memberEventListaners) {
            listaner.onMemberEnent(MemberEvent.UNBLOCKED, member);
        }
    }

    private boolean isGroupEmpty(Key key) {
        Group  group = groups.get(key);
        if (group != null) {
            return group.isGroupEmpty(key);
        }
        return false;
    }

}
