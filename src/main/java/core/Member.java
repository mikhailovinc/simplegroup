package core;

import java.util.Objects;

public class Member {
    private String id;
    private Object additionalData;
    private String role;

    public Member(String id) {
        this.id = id;
    }

    public void attachObject(Object o) {
        this.additionalData = o;
    }

    public String getId() {
        return id;
    }

    public Object getAdditionalData() {
        return additionalData;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return id.equals(member.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
