package core;

public interface State<R> {
    void update(Member issuer, Object data, Class clazz, Group group);
    R read(Member member);
}
