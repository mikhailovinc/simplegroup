package resources;

import core.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class SessionManagerResources {

    @POST
    @Path("createGroup")
    @Produces(MediaType.APPLICATION_JSON)
    public Key createGroup(@QueryParam("strategy") Strategy strategy) {
        if (strategy == null) return null;

        GroupManager manager = GroupManager.getInstance();

        return manager.createGroup(strategy, new State() {
            @Override
            public void update(Member issuer, Object data, Class clazz, Group group) {

            }

            @Override
            public Object read(Member member) {
                return null;
            }
        });
    }

    @POST
    @Path("joinGroup/{user}")
    public boolean joinGroup(@PathParam("user") String user, Key groupId) {
        GroupManager manager = GroupManager.getInstance();

        return manager.joinGroup(groupId, new Member(user));
    }

    @DELETE
    @Path("removeGroup")
    public Response removeGroup(Key key) {
        GroupManager manager = GroupManager.getInstance();
        manager.removeGroup(key);

        return Response.ok().build();
    }

}
