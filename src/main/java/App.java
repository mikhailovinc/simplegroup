/*
 * This Java source file was generated by the Gradle 'init' task.
 */

import auth.LoginService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.HandlerWrapper;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import resources.SessionManagerResources;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {

    private static final String[] DEFAULT_ROLES = { "user", "admin" };

    public static void main(String[] args) throws Exception {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");


        Server jettyServer = new Server(8080);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.classnames",
                SessionManagerResources.class.getCanonicalName());

        jerseyServlet.setInitParameter("javax.ws.rs.Application", "JerseyResourceConfig");


        try {
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }


    private static HandlerWrapper authenticationInit() {
        ConstraintSecurityHandler security = new ConstraintSecurityHandler();

        Constraint constraint = new Constraint();
        constraint.setName("auth");
        constraint.setAuthenticate(true);
        constraint.setRoles(DEFAULT_ROLES);

        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setPathSpec("/auth");
        mapping.setConstraint(constraint);

        security.setConstraintMappings(Collections.singletonList(mapping));
        security.setAuthenticator(new BasicAuthenticator());
        security.setLoginService(new LoginService());

        security.setHandler(new TestHandler());

        return security;
    }

    private static class TestHandler extends AbstractHandler {

        @Override
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            String userName = request.getUserPrincipal().getName();

            List<String> roles = new ArrayList<>();

            for (String role: DEFAULT_ROLES) {
                if (request.isUserInRole(role)) {
                    roles.add(role);
                }
            }

            TokenManager tokenManager = new TokenManager("secret");

            String token = tokenManager.getToken(userName, roles.toArray(new String[0]));

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode node = mapper.createObjectNode();
            node.put("status", "OK");
            node.put("token", token);

            response.setContentType("application/json");

            response.setStatus(HttpServletResponse.SC_OK);
            mapper.writerWithDefaultPrettyPrinter().writeValue(response.getWriter(), node);

            baseRequest.setHandled(true);
        }

        @Override
        public String dumpSelf() {
            return null;
        }
    }

   /* public static void main(String[] args) {

        EntityManagerFactory sessionFactory = Persistence.createEntityManagerFactory( "Hibernate" );
        EntityManager entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Credentials cred1 = entityManager.find(Credentials.class, "denis");
        Credentials cred2 = entityManager.find(Credentials.class, "dmitry");
        Credentials cred3 = entityManager.find(Credentials.class, "dmitr");
        entityManager.remove(cred2);


        entityManager.getTransaction().commit();
        entityManager.close();
        sessionFactory.close();

        System.out.println(" ====================== ");
        System.out.println(cred1);
        System.out.println(cred2);
        System.out.println(cred3);
        System.out.println(" ====================== ");

    }*/

}
