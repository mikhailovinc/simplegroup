import auth.AuthorizationFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class JerseyResourceConfig extends ResourceConfig {
    public JerseyResourceConfig() {
        register(RolesAllowedDynamicFeature.class);
        register(AuthorizationFilter.class);
    }
}
