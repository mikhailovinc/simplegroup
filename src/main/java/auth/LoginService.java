package auth;

import org.eclipse.jetty.security.AbstractLoginService;
import org.eclipse.jetty.util.security.Password;

public class LoginService extends AbstractLoginService {

    @Override
    protected String[] loadRoleInfo(UserPrincipal user) {
        return new String[] {"user"};
    }

    @Override
    protected UserPrincipal loadUserInfo(String username) {
        return new UserPrincipal("denis", new Password("123"));
    }
}
