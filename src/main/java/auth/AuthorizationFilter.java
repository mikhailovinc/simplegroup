package auth;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.Principal;


@Authorized
@Provider
public class AuthorizationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {


        requestContext.setSecurityContext(new SecurityContext() {
            @Override
            public Principal getUserPrincipal() {
                return () -> null;
            }

            @Override
            public boolean isUserInRole(String role) {
                return false;
            }

            //TODO: get rid of this bullshit
            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return "Token-Based-Auth-Scheme";
            }
        });
        String header = requestContext.getHeaderString("Authorization");

        System.out.println(header);
        if (header == null) {
            Response.ResponseBuilder responseBuilder = Response.serverError();
            Response response = responseBuilder.status(Response.Status.BAD_REQUEST).build();

            requestContext.abortWith(response);
        }
    }
}
