import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

import java.util.ArrayList;
import java.util.List;

public class TokenManager {
    Algorithm algorithm;

    public TokenManager(String secretKey) {
        algorithm = Algorithm.HMAC256(secretKey);
    }

    public String getToken(String user, String[] role) {
        String token = JWT.create()
                .withClaim("user", user)
                .withArrayClaim("role", role)
                .sign(algorithm);

        return token;
    }

    public void verefy() {

    }

}
